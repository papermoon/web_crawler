import scrapy


class MySpider(scrapy.Spider):
    name = "myspider"
    start_urls = ["https://www.purepc.pl/node?page=1",]

    def parse(self, response):
        #extracts links from a website
        for href in response.xpath("//div[contains(@class, 'nl_item')]/h2/a/@href").extract():
            yield response.follow(href, self.paginate_articles)
        #handles pagination of a website
        next_page = response.xpath("//li[contains(@class, 'pager-next')]/a/@href").extract_first()
        if next_page is not None:
            yield response.follow(next_page, self.parse)

    def paginate_articles(self, response):
        # handles pagination of a single article (if it has more than one page)
        page_menu = response.xpath('//div[contains(@class, "PageMenuList")]/ul/li/a/@href').extract()
        text = ''.join(page_menu)
        print(response.url)
        # if page_menu is None:
        # yield response.follow(response.url, self.parse_article)
        # else:
        #     for page in page_menu:
        #         yield response.follow(page, self.parse_article)

    def parse_article(self, response):
        self.log(response.url)
        name = response.url.split("/").pop()
        print(name)
        file = open("{}".format(name),"a")
        result = response.xpath('//div[contains(@class, "content clear-block")]/p/text()').extract()
        text_of_article = ''.join(result)
        print(text_of_article)
        file.write(text_of_article)
        file.close()
        # comment

